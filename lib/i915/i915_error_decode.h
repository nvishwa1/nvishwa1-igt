/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2022 Intel Corporation
 */
#ifndef _I915_ERROR_DECODE_H_
#define _I915_ERROR_DECODE_H_

#include <stdbool.h>
#include <stdint.h>

unsigned long
i915_ascii85_decode(char *in, uint32_t **out, bool inflate, char **end);

#endif /* _I915_ERROR_DECODE_H_ */
