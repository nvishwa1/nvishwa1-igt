// SPDX-License-Identifier: MIT
/*
 * Copyright © 2022 Intel Corporation
 */

#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>

#include "ioctl_wrappers.h"
#include "i915_drm.h"
#include "i915_drm_local.h"
#include "i915_vm_bind.h"

void i915_vm_bind(int i915, uint32_t vm_id, uint64_t va, uint32_t handle,
		  uint64_t offset, uint64_t length, uint64_t flags,
		  uint32_t syncobj, uint64_t fence_value)
{
	struct drm_i915_gem_vm_bind bind;

	memset(&bind, 0, sizeof(bind));
	bind.vm_id = vm_id;
	bind.handle = handle;
	bind.start = va;
	bind.offset = offset;
	bind.length = length;
	bind.flags = flags;
	if (syncobj) {
		bind.fence.handle = syncobj;
		bind.fence.value = fence_value;
		bind.fence.flags = I915_TIMELINE_FENCE_SIGNAL;
	}

	gem_vm_bind(i915, &bind);
}

void i915_vm_unbind(int i915, uint32_t vm_id, uint64_t va, uint64_t length)
{
	struct drm_i915_gem_vm_unbind unbind;

	memset(&unbind, 0, sizeof(unbind));
	unbind.vm_id = vm_id;
	unbind.start = va;
	unbind.length = length;

	gem_vm_unbind(i915, &unbind);
}

int i915_vm_bind_version(int i915)
{
	struct drm_i915_getparam gp;
	int value = 0;

	memset(&gp, 0, sizeof(gp));
	gp.param = I915_PARAM_VM_BIND_VERSION;
	gp.value = &value;

	ioctl(i915, DRM_IOCTL_I915_GETPARAM, &gp, sizeof(gp));
	errno = 0;

	return value;
}
