/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2022 Intel Corporation
 */
#ifndef _I915_VM_BIND_H_
#define _I915_VM_BIND_H_

#include <stdint.h>

void i915_vm_bind(int i915, uint32_t vm_id, uint64_t va, uint32_t handle,
		  uint64_t offset, uint64_t length, uint64_t flags,
		  uint32_t syncobj, uint64_t fence_value);
void i915_vm_unbind(int i915, uint32_t vm_id, uint64_t va, uint64_t length);
int i915_vm_bind_version(int i915);

#endif /* _I915_VM_BIND_ */
